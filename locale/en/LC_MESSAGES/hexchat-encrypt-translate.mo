��          �      <      �  <   �  <   �  >   +  =   j  <   �  <   �  >   "  =   a  +   �  !   �  $   �  W     :   j  5   �  ;   �       #   0  �  T  =   �  =   1  ?   o  >   �  =   �  =   ,  ?   j  >   �  *   �      	  #   5	  W   Y	  9   �	  4   �	  :    
     [
  "   s
     
                      	                                                                          Additional display of original messgetext disbled (globally) Additional display of original messgetext enabled (globally) Additional display of original messgetext was already disabled Additional display of original messgetext was already enabled Additional sending of original messgetext disbled (globally) Additional sending of original messgetext enabled (globally) Additional sending of original messgetext was already disabled Additional sending of original messgetext was already enabled Apply translation on incomming and outgoing Only translate incomming messages Show original untranslated incomming This will breake privacy for messages which were sent or received encrypted originally. Translation enabled for incomming messages only (globally) Translation switched to »only incomming« (globally) Translation was already enabled for incomming messages only incomming messages only. untranslated incomming message text Project-Id-Version: HexChat Encrypt Translate 2.0.6
Report-Msgid-Bugs-To: forum.antiXlinux.com
PO-Revision-Date: 2021-11-13 19:15+0000
Last-Translator: Robin, 2021
Language-Team: English (https://www.transifex.com/antix-linux-community-contributions/teams/120110/en/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Additional display of original messagetext disbled (globally) Additional display of original messagetext enabled (globally) Additional display of original messagetext was already disabled Additional display of original messagetext was already enabled Additional sending of original messagetext disbled (globally) Additional sending of original messagetext enabled (globally) Additional sending of original messagetext was already disabled Additional sending of original messagetext was already enabled Apply translation on incoming and outgoing Only translate incoming messages Show original untranslated incoming This will breach privacy for messages which were sent or received encrypted originally. Translation enabled for incoming messages only (globally) Translation switched to »only incoming« (globally) Translation was already enabled for incoming messages only incoming messages only. untranslated incoming message text 