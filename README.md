# HexChat Encrypt Translate

- **Symmetric encryption between users of the HexChat IRC client** ([Homepage](https://hexchat.github.io/))
- **Two way translation of chat contents from/to about 100 languages**


This plugin is meant for easy multilanguage access to the IRC translator channel of antiX translators (_#antiX-translators_ on IRC Network _Libera.Chat_), but it may be useful for other people also. Main purpose is to translate automatically from the native language of the current user to the default chat language and vice versa immediately, making it possible for people with poor knowledge of the channel language (but good knowledge in their native language) to participate.
Moreover it provides an improvement of privacy for 1:1 conversation in private context by means of symmetric encryption.


**Installation using Debian installer packages**

1. Download the most recent .deb file from the installer packages subfolder, along with its checksum file.
2. enter in a virtual terminal window like e.g. RoxTerm the command
`shasum -c hexchat-encrypt-translate.deb.sha256.sum`
3. In case it comes up with an "OK", enter
`sudo apt-get install hexchat-encrypt-translate.deb`
to install the package. Watch carefully for any errors in the console output of this command.


**Alternative manual Installation**

To keep it simple without the need of installing external python libraries the script uses the _openssl_ command line client ([Documentation](https://wiki.openssl.org/index.php/Manual:Enc(1))) for encryption/decryption and the _translate-shell_ client ([Documentation](<https://github.com/soimort/translate-shell/wiki>)) for translation, both of them available in antiX, along with preinstalled HexChat. For enhanced convenience you may want to install _pwgen_ ([Homepage](https://github.com/tytso/pwgen)) also, this will allow you to generate and store safe keys automatically using the _/enc makekey_ command from within HexChat.

1. Place the file <i>enc.py</i> in the subfolder _addons_ of HexChats configuration directory (e.g. _~/.config/hexchat/addons_.)
2. Place the respective language file _(.mo type)_ for your language from one of the locale subdirectories in the system folder _/usr/share/locale/‹your_language_ID›/LC_MESSAGES_. 
3. Make sure you have installed _openssl_ (>= 1.1.1d-0), _translate-shell_ (>= 0.9.6.11) and _pwgen_ (>= 2.0.8-1) on your system.
4. Since the hexchat python interface is no longer integrated in the debian hexchat package, you possibly will have to install the package _hexchat-python3_ (>= 2.14.2-4) also before you can use HexChat-Encrypt-Translate. (For older versions of this script up to 2.0.6 you'll have to use the _hexchat-python2_ package instead)


**User interface language support**

The User interface of this script was translated to more than 100 languages, most of them are automatic translations which need some manual improvement. In case you find the wording or grammar inadequate, please visit [antiX-linux community contributions at transifex](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/hexchat-encrypt-translate/) to improve the translation of the user interface in your language. Signing up for editing the translation strings at transifex is for free.


**Usage instructions**

After starting up HexChat the HexChat-Encrypt-Translate pluguin should come up with the message, e.g.

    Hexchat-Encrypt-Translate Ver. 1.8.2 

(or any other version number), along with some status information.

Disable/Enable outgoing encryption with the commands _/enc enable_ and  _/enc disable_. If not enabled when receiving an ecrypted private message, encryption will get enabled automatically for this conversation. Text changes to green.
Enter _/enc_ for instructions how to create and manage encryption keys.
Hint: It is a good idea to create strong passphrases and exchange them via a second path (outside IRC, possibly outside internet) with your interlocutors.
The encryption is meant to make a privacy level available on 1:1 conversations, equal to that of a snailmail letter, which normally can't be read by mail service provider on transport since it has an envelope. So think of this feature as a simple letter envelope. The symmetric encryption used here is not configured for high security encryption.

Disable/Enable translation of messages using the commands _/tra in_, _/tra out_ or _/tra both_ and _/tra off_. Translation is enabled by default. Also you can decide whether you want to have the original untranslated text sent or displayed additionally.  See _/tra_ for a survey of all available commands. The home language defaults to your system user interface language, the foreign language defaults to English. You can change both of them using the commands _/tra sethome ‹ll_CC›_ and _/tra setforeign ‹ll_CC›_, where _‹ll_CC›_ is to be replaced by the language identifier referring to language in Unix style. Examples: English: _en_, or _en_GB_, _en_US_, _en_AU_, _en_IE_ ... French: _fr_, or _fr_FR_, _fr_BE_ ..., Filipino: _fil_ or _fil_FIL_. Enter _/tra_ to see all accepted translation commands. The spectrum of supported languages may vary, depending on the translation service provider used by your system (google in most cases). Set the foreign language according to the language used in channel.

Questions, suggestions and bug reporting for the recent version of _HexChat Encrypt Translate_ please to [antiX forums](https://www.antixforum.com/), section »Translation«, or at [gitlab](https://gitlab.com/Robin-antiX/hexchat-encrypt-translate)

Written 2021/2022 by Robin.antiX for the antiX community.

**System reqirements:**
The plugin was written and tested on [antiX Linux](https://antixlinux.com) 19, using HexChat version 2.14.2, openssl version 1.1.1d, translate-shell version 0.9.6.11-1 and will probably run on any linux or newer HexChat versions.
It also might be able to run on MS Windows or Apple Systems, if at least the helper programs »openssl« and »translate-shell« are installed, possibly via »WSL«, »Cygwin«, or »MSYS2«, or functionally identical windows/apple versions of these helper programs, but this is still untested.

**Additional information and resources**
- HexChat Python Interface: [Documentation](http://hexchat.readthedocs.io/en/latest/script_python.html#autoloading-modules)
- Original 1.0.0 Version of hexchat-enc by Robert Kvant: [Download and Repository](https://github.com/robertkvant/hexchat-encrypt/blob/cfb4e969e6c05eaae74e34da2a14075d48c270d4/enc.py)

**License:**
GPL Version 3 (GPLv3) for all recent Versions of _HexChat Encrypt Translate_. The original parts of code as of _Hexchat Encrypt_ Ver. 1.0.0 are available under MIT license.

------
Robin.antiX, 2021,2022
