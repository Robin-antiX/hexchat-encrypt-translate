#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  »hexchat-encrypt-translate«
#
#  hexchat plugin for encryption of private messages and files sent
#  and/or received, and for translation of all messages sent and/or
#  received.
#
#  The GPL License Version 3 (GPLv3)
#
#  Copyright (c) 2021, 2022 Robin.antiX
#  Copyright (c) 2021, 2022 the antiX comunity
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the  Free Software Foundation, either  version 3  of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program.  If not, see
#
#  <http://www.gnu.org/licenses/>.

# This is derived work.
# GPL v3 applys to all additions and modifications in this code as of the
# year 2021 onwards (starting with »hexchat-encrypt-translate« version 1.0.1)

# The included original code of »hexchat-encrypt« version 1.0.0 is licensed
# under MIT License:

# The MIT License (MIT)
# 
# Copyright (c) [2016] [Robert Kvant]
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE

# You may obtain the original code of »hexchat-encrypt« version 1.0.0 from
# https://github.com/robertkvant/hexchat-encrypt/blob/cfb4e969e6c05eaae74e34da2a14075d48c270d4/enc.py

# The recent code of »hexchat-encrypt-translate« is available at
# https://gitlab.com/Robin-antiX/hexchat-encrypt-translate

# HexChat Python Interface Documentation
# https://hexchat.readthedocs.io/en/latest/script_python.html

# Modified by Robin.antiX 4.2021 (compliance uptdate to openssl 1.1.1d, + changed default behaviour to "enable" in private dialogs.)
# Modified by Robin.antiX 6.2021 (added translation functionality for international usage)
# Modified by Robin.antiX 8.2021 (encryption: bind keys per nick, so you can use more than one keys for private 1:1 conversation in different private windows.)
# Modified by Robin.antiX 9.2021 (retrieve correct path to recently used configuration directory of hexchat if hexchat was called with -d option, in order to find the keyfiles for encryption.)
# Modified by Robin.antiX 10.2021 (added method for displaying original message for incomming and outgoing translations, also useful for channel participants without this plugin)
# Modified by Robin.antiX 10.2021 (added manual override of target and source language eg. by "tra set fr_BE pt_BR", accepting unix style language identifiers.)
# Modified by Robin.antiX 10.2021 (use (if present) login name (registered user) instead of nick for selection of encryption key.)
# Modified by Robin.antiX 11.2021 (prepared script for gettext translation)
# Modified by Robin.antiX 3/2022 (updated code from 2.7 to run with python 3 for antiX 21)

# todo: async processing of hook events to avoid first encrypted incomming message not to get decrypted, if private window opens only the same moment. 
# todo: allow channel specific language settings
# todo: add encryption to file transfer and DCC also.
# todo: add alternative asymetric encryption using certificates.
# todo: add option to display English (or channel-) language also.
# todo: save individual settings in a settings file or in hexchatprefs and restore them on loading (eg per-channel language settings).

# known bugs: -- first incomming encrypted message is not decrypted, in case hook signal of opening private dialog window is not registered before hook signal for incomming message was processed. Timing problem, needs async processing available only in python 3.x
# -- if passphrase user enters contains a doublequote (") this has to be doubled (enter "" instead), since hexchat eats up this character otherwise and decryption fails.

import hexchat
import subprocess
import os
import re
import glob
import gettext

_ = gettext.gettext
__module_name__ = "hexchat-encrypt-translate" 
__module_version__ = "2.0.7"
__module_description__ = "Hexchat " + _("symmetric encryption and translation")

PROCESSING = False
ACCOUNTS = dict()			# This is the translator from nick to account
DIALOGS = set()				# This is a set of lists, taking (channel, server) in each item.
EXCLUDES = set()			# same as DIALOGES
DEBUG = False				# Default setting for debug option.
MCHARSIZE = 250				# Chunk size for not damaging cryptograms on transmission
TRANSLATING = True			# Default setting for translation.
SHOWORIGINAL = True		# Default setting for display of original untranslated text.
TRANSMITORIGINAL = True	# Default setting for sending of original untranslated text.
TRANSDIRECTION = "both"  	# Default setting for translation direction. Valid: both, in, out
FOREIGNLANG = "en"			# default is "en" for channel compatibility reasons
account = str()
hook1 = set()
hook2 = set()
hook3 = set()
hook4 = set()
hook5 = set()
hook6 = set()
hook7 = set()
hook8 = set()
hook9 = set()
hook10 = set()
hook11 = set()
noenc = False
notrans = False
nopwgen = False

gettext.bindtextdomain("hexchat-encrypt-translate","/usr/share/locale")
gettext.textdomain("hexchat-encrypt-translate")

""" check whether a needed tool is not installed """
def ToolsExists(toolname):
	exitcode = subprocess.call(['which', toolname])
	if exitcode != 0:
		return False
	return True

""" write new key to keyfile """
def schreibeEintrag(schluesselname,schluesseleintrag):
	if not os.path.exists(PASSDIR):
		os.makedirs(PASSDIR)
	with open(PASSDIR + "/" + schluesselname + ".key", 'w') as schluesseldatei:
		schluesseldatei.write(schluesseleintrag)
		
""" Create safe key for encryption """
def newPassphrase():
	process = subprocess.Popen(
		["pwgen","-cyns1","60","1"],
		stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
	stdout,stderr = process.communicate()      
	if process.returncode == 0: return stdout # return translated to foreign language
	raise Exception(stderr)

""" Create new keyfile automatically for recent dialog window """
def MakeKey(ctxt):
	try:
		if isDialog(ctxt):
			schluesselname = str(ACCOUNTS.get(ctxt.get_info('channel')))
			schluesseleintrag = newPassphrase()
			schreibeEintrag(schluesselname,schluesseleintrag)			
			ctxt.prnt("\002" + _("The new key for conversation with") + " " + schluesselname + " " + _("is") + ":\n\n" + schluesseleintrag)
			ctxt.prnt("\n\002" + _("A key file was created.") + "\n" + _("Please transmit this key line via a secure external path to the interlocutor. After she/he has entered this line using") + " »/enc setkey« " + _("command you no longer need to send plaintext via internet."))
		else:
			ctxt.prnt(textNeg(_("Automatically creating keys is only possible in private dialog windows. Please use") + " »/enc key« " + _("to manually create a new keyfile from a passphrase.")))
	except Exception:
		ctxt.prnt(_("Can't create a new key. Possibly the") + " »pwgen« " + _("tool is not installed on your system."))	

""" Set new Key for recent dialog window """
def	SetKey(ctxt,schluesseleintrag):
			if isDialog(ctxt):	
				schluesselname = str(ACCOUNTS.get(ctxt.get_info('channel')))
				schreibeEintrag(schluesselname,schluesseleintrag)								
				ctxt.prnt("\002" + _("The new keyfile for conversation with") + " " + schluesselname + " " + _("was created from the key line entered. Make sure your interlocutor uses the same key line."))
			else:
				ctxt.prnt(textNeg(_("The") + " /enc setkey " +_("command can only be called from within a private window. Use") + " /enc key " + _("instead") + "."))

""" Enter new Key manually """
def EnterKey(ctxt,schluesselname,schluesseleintrag):
	schreibeEintrag(schluesselname,schluesseleintrag)
	ctxt.prnt(_("A new key file for") + " " + schluesselname + " " + _("was created."))
	return hexchat.EAT_ALL

""" Show key of given nick """
def ShowKey(ctxt,nick):
	try:
		with open(PASSDIR + "/" + nick + ".key", 'r') as schluesseldatei:
			ctxt.prnt(_("The key for conversation with") + " " + nick + " " + _("is:") + "\n" + schluesseldatei.readline())
	except:
		ctxt.prnt(_("No Key for") + " " + nick + " " + _("found. Try") + (" »/enc status« ") + _("to get a survey of all keys stored."))
	return hexchat.EAT_ALL

""" separate original string from english string and translate, build up complete hexchat display string """
def trennen(message, mode):
	message_u = re.sub('.+ — \(ORIGINAL:.+ »(.+)«\)$', r'\1', message)			# extract original (untranslated) message
	message_e = re.sub('(.+) — \(ORIGINAL:.+ ».+«\)$', r'\1', message)			# extract english chatroom translation
	message_h = os.linesep.join([s for s in uebersetzen(message_e,FOREIGNLANG,HOMELANG).splitlines() if s.strip()])	# translate board language to user home language.
	if not SHOWORIGINAL:
		message = textDefault(message_h)	# show translated message only, no green color since it was sent unencrypted via internet to translation service provider.
	elif mode == "enc":
		message = textDefault(message_h) + "\n" + textPos(message_u) # + "\n" + textTrans(message_e)	# build up display message string
	else:
		message = textDefault(message_h) + "\n" + textTrans(message_u) # + "\n" + textTrans(message_e)	# build up display message string
	return message

""" translate 'text' through the trans command line client """
def uebersetzen(text,ein,aus):
	process = subprocess.Popen(
		["trans","-s",ein,"-t",aus,"-b","--show-original","n","--show-languages","n","--show-original-dictionary","n","--show-dictionary","n","--show-alternatives","n","--show-prompt-message","n","--show-original-phonetics","n","--show-translation-phonetics","n","--show-translation","y","--no-ansi","--no-browser","--no-view","--"],
		stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
	stdout,stderr = process.communicate(text)      
	if process.returncode == 0: return re.sub(r'u200b', '', stdout) # return translated to foreign language, remove odd characters left over from zero width blanks added by translation service provider not caught by translate-shell
	raise Exception(stderr)

""" Encrypt 'plaintext' through the openssl command line client """
def encrypt(plaintext,schluesselname,ctxt):
	schluesseldatei = PASSDIR + "/" + schluesselname + ".key"
	if DEBUG:
		ctxt.prnt("Using Keyfile: " + schluesseldatei)
	process = subprocess.Popen(
		["openssl","enc","-aes-256-cbc","-iter","5000","-e","-salt","-a","-A","-pass","file:" + schluesseldatei],
		stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
	stdout,stderr = process.communicate(plaintext)      
	if process.returncode == 0: return stdout # Return as base64
	raise Exception(stderr)

""" Decrypt 'cryptogram' through the openssl command line client """
def decrypt(cryptogram,schluesselname,ctxt):
	schluesseldatei = PASSDIR + "/" + schluesselname + ".key"
	if DEBUG:
		ctxt.prnt("Using Keyfile: " + schluesseldatei)
	process = subprocess.Popen(
		["openssl","enc","-aes-256-cbc","-iter","5000","-d","-salt","-a","-A","-pass","file:" + schluesseldatei],
		stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf8')
	stdout,stderr = process.communicate(cryptogram)
	if process.returncode == 0: return stdout
	raise Exception(stderr)

""" Invoked every time a user sends a message """
def send(word, word_eol, userdata):
	ctxt = hexchat.get_context()
	""" Only encrypt outgoing message if channel/server 
	is added to DIALOGS list """
	if channelServer(ctxt) in DIALOGS:
		try:
			message = word_eol[0]
			""" translate outgoing message, if translation was enabled """
			if TRANSLATING and TRANSDIRECTION != "in":
				if TRANSMITORIGINAL:
					message_t = os.linesep.join([s for s in uebersetzen(message,HOMELANG,FOREIGNLANG).splitlines() if s.strip()]) + " — (ORIGINAL:" + re.sub('-' ,'_' , HOMELANG) + " »" + message + "«)"
				else:
					message_t = os.linesep.join([s for s in uebersetzen(message,HOMELANG,FOREIGNLANG).splitlines() if s.strip()])
				""" To avoid the encrypted text gets cut off during transmission
				encrypt and send the message in chunks of MCHARSIZE character-
				length . """
			else:
				message_t = message
			
			target = ctxt.get_info('channel')
			account = ACCOUNTS.get(target)
			for x in range(0,len(message_t),MCHARSIZE): 
				""" To mark the message as encrypted, 'HEXCHATENC:' is 
				concatenated to the encrypted message. """
				hexchat.command('PRIVMSG %s :%s' % 
					(target, "HEXCHATENC:" 
						+ encrypt(message_t[x:x+MCHARSIZE],account,ctxt)))
			""" Message sent, print to dialog window"""
			hexchat.emit_print('Your Message', 
				hexchat.get_info('nick'), textOwn(message))
			return hexchat.EAT_HEXCHAT
		except Exception as e:
			ctxt.prnt(textNeg(_("Could not encrypt!")))
			if DEBUG: ctxt.prnt(str(e))
			return hexchat.EAT_ALL
	else:
		message = word_eol[0]
		""" translate outgoing message, if translation was enabled """
		if TRANSLATING and TRANSDIRECTION != "in":
			if TRANSMITORIGINAL:
				message_t = os.linesep.join([s for s in uebersetzen(message,HOMELANG,FOREIGNLANG).splitlines() if s.strip()]) + " — (ORIGINAL:" + re.sub('-' ,'_' , HOMELANG) + " »" + message + "«)"
			else:
				message_t = os.linesep.join([s for s in uebersetzen(message,HOMELANG,FOREIGNLANG).splitlines() if s.strip()])
			hexchat.command('PRIVMSG %s :%s' % 
				(ctxt.get_info('channel'), message_t))
			""" Message sent, print to dialog window"""
			hexchat.emit_print('Your Message', 
				hexchat.get_info('nick'), textDefaultOwn(message))			
			return hexchat.EAT_HEXCHAT
		else:
			return hexchat.EAT_NONE
	return hexchat.EAT_NONE

""" Invoked every time a message is received in a private dialog """
def receive(word, word_eol, userdata):
	global PROCESSING
	if PROCESSING:
		return hexchat.EAT_NONE
	sender,message = word[0],word[1]
	ctxt = hexchat.get_context()
	""" If the first 11 characters of the received message 
	is the word 'HEXCHATENC:' the text is probably encrypted """
	if message[:11] == "HEXCHATENC:":
		try:
			account = ACCOUNTS.get(sender)
			plaintext = decrypt(message[11:],account,ctxt)
			""" If sender not in DIALOGS -> enable outgoing 
			encryption for this context """
			if channelServer(ctxt) not in DIALOGS:
				enable(ctxt)
			PROCESSING = True
			""" Message decrypted, translate it, if translation was enabled """
			if TRANSLATING and TRANSDIRECTION != "out":
				plaintext = trennen(plaintext,"enc")
			""" print to dialog window """
			ctxt.emit_print('Private Message to Dialog', 
				sender, textPos(plaintext))
			PROCESSING = False
			return hexchat.EAT_HEXCHAT
		except Exception as e:
			ctxt.prnt(textNeg(_("Could not decrypt!")))
			if not os.path.isfile(PASSDIR + "/" + str(account) + ".key"):
				ctxt.prnt(textNeg(_("No key found for") + " »" + str(account) + "«. " + _("Please exchange keys and use") + (" »/enc key« ") + _("commands before starting encryption.")))
			if DEBUG: ctxt.prnt(str(e))
	else:
			PROCESSING = True
			""" Translate message, if translation was enabled """
			if TRANSLATING and TRANSDIRECTION != "out":
				message=trennen(message,"plain")
			""" print to dialog window """
			ctxt.emit_print('Private Message to Dialog', 				# send complete translated+untranslated message back to hexchat
				sender, textDefault(message))
			PROCESSING = False
			return hexchat.EAT_HEXCHAT

""" Invoked every time a message is received in a channel """
def ChannelReceive(word, word_eol, userdata):
	global PROCESSING
	if PROCESSING:
		return hexchat.EAT_NONE
	sender,message = word[0],word[1]
	ctxt = hexchat.get_context()
	PROCESSING = True
	""" Translate message, if translation was enabled """
	if TRANSLATING and TRANSDIRECTION != "out":
		message=trennen(message,"plain")
	""" print to dialog window """
	ctxt.emit_print('Channel Message', 
		sender, message)
	PROCESSING = False
	return hexchat.EAT_HEXCHAT

""" Enable outgoing encryption on current channel """
def enable(ctxt):
	if isDialog(ctxt) and not noenc:
		nick = ctxt.get_info('channel')
		target = ACCOUNTS.get(nick)
		if target == None:					# in case we have no signal on hook since there was no whois answer for "account" we have to use nick instead.
			ACCOUNTS[nick] = nick
			target = nick
		if os.path.isfile(PASSDIR + "/" + str(target) + ".key"):
			if not channelServer(ctxt) in DIALOGS:
				DIALOGS.add(channelServer(ctxt))
			if channelServer(ctxt) in EXCLUDES:
				EXCLUDES.remove(channelServer(ctxt))
			ctxt.prnt(textPos(_("Encryption enabled in this dialog with") + " " + str(target) + "."))
		else:
			if channelServer(ctxt) in DIALOGS:
				DIALOGS.remove(channelServer(ctxt))
			if not channelServer(ctxt) in EXCLUDES:
				EXCLUDES.add(channelServer(ctxt))
			ctxt.prnt(textNeg(_("Encryption impossible.") + "\n" + _("No key found for") + " \002"
				+ str(target) + "\017. " + _("Please exchange keys and use") + " »/enc key« "
				+ _("commands before starting encryption.") + "\n\002"
				+ _("Until then all messages in this private dialog will be transferred as plain text via internet.")))
	else:
		ctxt.prnt(textNeg(_("Encryption can only be enabled on private dialog windows")))
	return hexchat.EAT_ALL

""" Disable outgoing encryption on current channel """
def disable(ctxt):
	if isDialog(ctxt):
		if channelServer(ctxt) in DIALOGS:
			DIALOGS.remove(channelServer(ctxt))
			ctxt.prnt(textNeg(_("Encryption disabled in this dialog")))
		else:
			ctxt.prnt(textNeg(_("Encryption was disabled already in this dialog")))
		if not channelServer(ctxt) in EXCLUDES:
			EXCLUDES.add(channelServer(ctxt))
	else:
		ctxt.prnt(textNeg(_("Encryption can only be enabled/disabled on private dialog windows")))
	return hexchat.EAT_ALL

""" Enable translation in both directions """
def EnableTranslationBoth(ctxt):
	global TRANSDIRECTION
	global TRANSLATING
	if TRANSLATING == False:
		ctxt.prnt(textPos(_("Translation enabled in both directions (globally)")))
		TRANSLATING = True
		TRANSDIRECTION = "both"
	else:
		if not TRANSDIRECTION == "both":
			ctxt.prnt(textPos(_("Translation switched to »both directions« (globally)")))
			TRANSDIRECTION = "both"
		else:
			ctxt.prnt(textPos(_("Translation was already enabled for both directions")))
	return hexchat.EAT_ALL

""" Enable translation for incomming messages """
def EnableTranslationIn(ctxt):
	global TRANSDIRECTION
	global TRANSLATING
	if TRANSLATING == False:
		ctxt.prnt(textPos(_("Translation enabled for incomming messages only (globally)")))
		TRANSLATING = True
		TRANSDIRECTION = "in"
	else:
		if not TRANSDIRECTION == "in":
			ctxt.prnt(textPos(_("Translation switched to »only incomming« (globally)")))
			TRANSDIRECTION = "in"
		else:
			ctxt.prnt(textPos(_("Translation was already enabled for incomming messages only")))
	return hexchat.EAT_ALL

""" Enable translation for outgoing messages """
def EnableTranslationOut(ctxt):
	global TRANSDIRECTION
	global TRANSLATING
	if TRANSLATING == False:
		ctxt.prnt(textPos(_("Translation enabled for outgoing messages only (globally)")))
		TRANSLATING = True
		TRANSDIRECTION = "out"
	else:
		if not TRANSDIRECTION == "out":
			ctxt.prnt(textPos(_("Translation switched to »only outgoing« (globally)")))
			TRANSDIRECTION = "out"
		else:
			ctxt.prnt(textPos(_("Translation was already enabled for outgoing messages only")))
	return hexchat.EAT_ALL

""" Disable Translation on current channel """
def disabletranslation(ctxt):
	global TRANSLATING
	if TRANSLATING == True:
		ctxt.prnt(textNeg(_("Translation disabled (globally)")))
		TRANSLATING = False
	else:
		ctxt.prnt(textNeg(_("Translation was disabled already")))
	return hexchat.EAT_ALL

""" Enable additional sending of untranslated messagetext """
def EnableTransOriginal(ctxt):
	global TRANSMITORIGINAL
	ctxt.prnt(textPos(_("Additional sending of original messgetext was already enabled") if TRANSMITORIGINAL else _("Additional sending of original messgetext enabled (globally)")))
	TRANSMITORIGINAL = True
	if not TRANSLATING:
		ctxt.prnt(textTrans(_("Translation is disabled. Use") + " /tra both|in|out " + _("to activate.")))
	return hexchat.EAT_ALL

""" Disble additional sending of untranslated messagetext """
def DisableTransOriginal(ctxt):
	global TRANSMITORIGINAL
	ctxt.prnt(textNeg(_("Additional sending of original messgetext disbled (globally)") if TRANSMITORIGINAL else _("Additional sending of original messgetext was already disabled")))
	TRANSMITORIGINAL = False
	if not TRANSLATING:
		ctxt.prnt(textTrans(_("Translation is disabled. Use") +" /tra both|in|out " + _("to activate.")))
	return hexchat.EAT_ALL

""" Enable additional display of untranslated messagetext """
def EnableOriginal(ctxt):
	global SHOWORIGINAL
	ctxt.prnt(textPos(_("Additional display of original messgetext was already enabled") if SHOWORIGINAL else _("Additional display of original messgetext enabled (globally)")))
	SHOWORIGINAL = True
	if not TRANSLATING:
		ctxt.prnt(_("Translation is disabled. Use") + " /tra both|in|out " + _("to activate."))
	return hexchat.EAT_ALL

""" Disble additional display of untranslated messagetext """
def DisableOriginal(ctxt):
	global SHOWORIGINAL
	ctxt.prnt(textNeg(_("Additional display of original messgetext disbled (globally)") if SHOWORIGINAL else _("Additional display of original messgetext was already disabled")))
	SHOWORIGINAL = False
	if not TRANSLATING:
		ctxt.prnt(_("Translation is disabled. Use") + " /tra both|in|out " + _("to activate."))
	return hexchat.EAT_ALL

""" Enable/disable verbose error messages """
def debug(ctxt):
	global DEBUG
	DEBUG ^= 1
	ctxt.prnt(_("Debug enabled") if DEBUG
		else _("Debug disabled"))
	return hexchat.EAT_ALL

""" Return channel and servername from the specified context """
def channelServer(ctxt):
	return (ctxt.get_info('channel'), ctxt.get_info('server'))

""" Returns true if current context is a dialog window """
def isDialog(ctxt):
	return [x.type for x in ctxt.get_list('channels') 
		if x.channel == ctxt.get_info('channel')][0] == 3

""" Return 'message' marked as translated """
def textTrans(message):
#	return "Translated: " + message
	return "\x0315" + message

""" Return 'message' as green text """
def textPos(message): 
	return "\x0303" + message

""" Return 'message' as green text """
def textOwn(message): 
	return "\x0309" + message
	
""" Return 'message' as green text """
def textDefaultOwn(message):
	return "\x0314" + message

""" Return 'message' as red text """
def textNeg(message): 
	return "\x0304" + message
	
""" Return 'message' as black text """
def textDefault(message):
	return "\x0301" + message	

""" Return 'message' as bold text """
def textBold(message): 
	return "\002"  + message

""" Handles all /enc command arguments """
def enc(word,word_eol,userdata):
	ctxt = hexchat.get_context()
	try:
		arg = word[1]
		if arg == "enable":
			enable(ctxt)
		elif arg == "disable":
			disable(ctxt)
		elif arg == "status":
			info(ctxt)
		elif arg == "key":
			EnterKey(ctxt,word[2],word[3])
		elif arg == "makekey":
			MakeKey(ctxt)
		elif arg == "setkey":
			SetKey(ctxt,word[2])
		elif arg == "showkey":
			try:
				nick = word[2]
			except:
				if isDialog(ctxt):
					nick = ACCOUNTS.get(ctxt.get_info('channel'))
				else:
					ctxt.prnt(_("Error: No Name was entered. see") + (" »/enc« ") + _("for help."))
					return hexchat.EAT_ALL
			ShowKey(ctxt,nick)
		elif arg == "debug":
			debug(ctxt)
		else:
			raise Exception
	except Exception:
		help(ctxt)
	return hexchat.EAT_ALL

""" Handles all /tra command arguments """
def tra(word,word_eol,userdata):
	global FOREIGNLANG
	global HOMELANG
	ctxt = hexchat.get_context()
	try:
		arg = word[1]
		if arg == "both":
			EnableTranslationBoth(ctxt)
		elif arg == "in":
			EnableTranslationIn(ctxt)
		elif arg == "out":
			EnableTranslationOut(ctxt)
		elif arg == "off":
			disabletranslation(ctxt)
		elif arg == "showorig":
			EnableOriginal(ctxt)
		elif arg == "noshoworig":
			DisableOriginal(ctxt)
		elif arg == "sendorig":
			EnableTransOriginal(ctxt)
		elif arg == "nosendorig":
			DisableTransOriginal(ctxt)
		elif arg == "setforeign":
			FOREIGNLANG = re.sub('_' ,'-' , word[2])
			ctxt.prnt(_("Foreign language set to") + " »" + re.sub('-' ,'_' , FOREIGNLANG) + "«.")
		elif arg == "sethome":
			HOMELANG = re.sub('_' ,'-' , word[2])
			ctxt.prnt(_("Home language set to") + " »" + re.sub('-' ,'_' , HOMELANG) + "«.")
		elif arg == "status":
			TransInfo(ctxt)		
		elif arg == "debug":
			debug(ctxt)
		else:
			raise Exception
	except Exception:
		help(ctxt)
	return hexchat.EAT_ALL

def EncErr(a,b,c):
	hexchat.prnt(textNeg(_("No encryption available.") + " " + _("Please install first") + (" »openssl«.")))
	return hexchat.EAT_ALL
	
def TraErr(a,b,c):
	hexchat.prnt(textNeg(_("No translation available.") + " " + _("Please install first") + (" »translate-shell«.")))
	return hexchat.EAT_ALL

""" start encryption when new private window is opened """
def NewPrivate(word,word_eol,userdata):
	global recent_ctxt
	ctxt = hexchat.get_context()
	nick = ctxt.get_info('channel')
	if isDialog(ctxt) and not ACCOUNTS.get(nick):
		ACCOUNTS[nick] = nick					# store nick as account by now, will get updated by result of whois in case ther is an account.
		recent_ctxt = ctxt
		GetAccountName(ctxt)					# retrieve account name
	return hexchat.EAT_ALL

def ClosePrivate(word,word_eol,userdata):
	global ACCOUNTS
	ctxt = hexchat.get_context()
	if isDialog(ctxt) and ACCOUNTS.get(ctxt.get_info('channel')):
		ACCOUNTS.pop(ctxt.get_info('channel'))
	return hexchat.EAT_NONE

""" store account name from whois output in list """
def StoreAccountName(word, word_eol, userdata):
	global ACCOUNTS
	global recent_ctxt
	account = word[2]						# extract accountname from hexchat whois list
	nick = recent_ctxt.get_info('channel')			# get nickname of interlocutor
	if account:
		ACCOUNTS[nick] = account				# store accountnames correlated to a nick in a dictionary
		recent_ctxt.prnt(_("You are talking to: ") + " \002" + str(nick) + "\017, " + _("Account:") + " \002" + str(account) + "\017")
	else:
		ACCOUNTS[nick] = nick					# store nickname instead to make translation from nick to account easy
		recent_ctxt.prnt(_("You are talking to: ") + " \002" + str(nick) + "\017, " + "not logged in with account.")
	enable(recent_ctxt)
	return hexchat.EAT_ALL

""" update account name dictionary on nick changes """
def UpdateAccountName(word, word_eol, userdata):
	global ACCOUNTS
	nick = word [1]
	oldnick = word[0]
	account = None
	if ACCOUNTS.get(oldnick):
		account = ACCOUNTS.get(oldnick)
		ACCOUNTS.pop(oldnick)					# remove outdated correlation
		ACCOUNTS[nick] = account				# add recent correlation
	if DEBUG:
		hexchat.prnt("Old Nick: " + str(oldnick) + "\nNew Nick: " + str(nick) + "\nAccount Name: " + str(account))
	return hexchat.EAT_NONE

def EatAll(a,b,c):			# eat all hooked whois events from whois messages hooked by GetAccountName()
	return hexchat.EAT_ALL

def EatLast(a,b,c):		#  eat last hooked event after end-of-whois message from server and let unhook all events hooked by GetAccountName()
	UnhookAll()
	return hexchat.EAT_ALL

def UnhookAll():			# unhook all the hooked whois events hooked by GetAccountName()
	hexchat.unhook(hook1)
	hexchat.unhook(hook2)
	hexchat.unhook(hook3)
	hexchat.unhook(hook4)
	hexchat.unhook(hook5)
	hexchat.unhook(hook6)
	hexchat.unhook(hook7)
	hexchat.unhook(hook8)
	hexchat.unhook(hook9)
	hexchat.unhook(hook10)
	hexchat.unhook(hook11)

def GetAccountName(ctxt):	# hook all events concerned by whois command to avoid the result to get printed, and send whois command.
	global hook1
	global hook2
	global hook3
	global hook4
	global hook5
	global hook6
	global hook7
	global hook8
	global hook9
	global hook10
	global hook11
	hook9 = hexchat.hook_print("WhoIs Real Host", EatAll)
	hook2 = hexchat.hook_print("WhoIs Away Line", EatAll)
	hook3 = hexchat.hook_print("WhoIs Channel/Oper Line", EatAll)
	hook4 = hexchat.hook_print("WhoIs End", EatLast)
	hook5 = hexchat.hook_print("WhoIs Identified", EatAll)
	hook6 = hexchat.hook_print("WhoIs Idle Line", EatAll)	
	hook7 = hexchat.hook_print("WhoIs Idle Line with Signon", EatAll)
	hook8 = hexchat.hook_print("WhoIs Name Line", EatAll)
	hook9 = hexchat.hook_print("WhoIs Real Host", EatAll)
	hook10 = hexchat.hook_print("WhoIs Server Line", EatAll)
	hook11 = hexchat.hook_print("WhoIs Special", EatAll)
	hook1 = hexchat.hook_print("WhoIs Authenticated", StoreAccountName)
	nick = ctxt.get_info('channel')
	hexchat.command("whois " + nick)	

""" Initialization"""
def init():
	global PASSDIR
	global TRANSLATING
	global HOMELANG
	global noenc
	global notrans
	try:
		hexchat.prnt(textPos("Hexchat-Encrypt-Translate Ver." + __module_version__))
		if not ToolsExists('trans'):
			hexchat.prnt(textNeg(_(" ! The helper tool") + " »translate-shell« " + _("was not found.") + " " + _("Please install first" + ".")))
			notrans = True
		if not ToolsExists('openssl'):
			hexchat.prnt(textNeg(_(" ! The helper tool") + " »openssl« " + _("was not found.") + " " + _("Please install first" + ".")))
			noenc = True
		if noenc & notrans:
			scriptname = str(os.path.basename(__file__))
			hexchat.prnt(_("Unloading") + " " + scriptname)
			hexchat.command('timer 1 unload ' + scriptname)		# using 1 second here; 0,1 seconds would be enough already to make sure script is loaded before unloading, but decimal values won't work here since they are language sensitive in hexchat versions before fix https://github.com/hexchat/hexchat/commit/8443755772160e61679e3122190da18ba10d8878
		else:
			if not ToolsExists('pwgen'):
				nopwgen = True
				hexchat.prnt(textNeg(_(" ! The helper tool") + " »pwgen« " + _("was not found.") + " " + _("Please install first" + ".")))			
			if not noenc: hexchat.prnt(textPos(_("Encryption enabled by default in private dialogs.")))
			if not notrans: hexchat.prnt(textPos(_("Translation enabled globaly by default.")))
			hexchat.prnt(textPos(_("Use") + " „/enc” " + _("or") + " „/tra” " + _("to obtain information about how to disable.")))
			if not notrans:
				hexchat.hook_command('tra', tra, help=_("For help use the command") + " „/tra”")
			else:
				TRANSLATING = False
				hexchat.hook_command('tra', TraErr, help=_("For help use the command") + " „/tra”")
			if not noenc:
				hexchat.hook_command('enc', enc, help=_("For help use the command") + " „/enc”")
				hexchat.hook_print('Open Context', NewPrivate)
				hexchat.hook_print('Close Context', ClosePrivate)
				hexchat.hook_print('Change Nick', UpdateAccountName)
			else:
				hexchat.hook_command('enc', EncErr, help=_("For help use the command") + " „/enc”")
			hexchat.hook_command('', send)
			hexchat.hook_print('Private Message to Dialog', receive)
			hexchat.hook_print('Channel Message', ChannelReceive)
			PASSDIR = hexchat.get_info("configdir") + "/hexchat-enc-keys"
			HOMELANG = re.sub('([a-z]{2,3})_([A-Z]{2,3})\..+$', r'\1-\2', os.getenv('LANG'))
	except Exception as e:
		hexchat.prnt(textNeg(str(e)))	

""" Print info about encryption/debug/passfiledirectory """
def info(ctxt):
	ctxt.prnt("\n" + textBold("----------- " + _("Encryption Status") + " -----------"))
	ctxt.prnt("* " + _("Passfiles stored in:") + " " + PASSDIR + "\n  " + _("Keys available for private communication with:"))
	ctxt.prnt("    " + "\n    ".join([re.sub('.+/(.+).key$', r'\1', nick) for nick in (glob.glob(PASSDIR + "/*.key"))]))
	ctxt.prnt("* " + _("Encryption") + " \x0303" + _("enabled") + "\x0301 " + _("in current private window") if channelServer(ctxt) in 
		DIALOGS else "* " + _("Encryption") + " \x0304" + _("disabled") + "\x0301 " + _("in current window"))
	if DIALOGS:
		ctxt.prnt("* " + _("The conversation in following private windows is encrypted:"))
		#ctxt.prnt("    " + "\n    ".join(str(x for x in DIALOGS)))
		for x in DIALOGS:
			(channel, server) = x 
			ctxt.prnt("    " + str(channel))
	else:
		ctxt.prnt("* " + _("Currently no dialog window uses encryption."))
	if DEBUG:
		ctxt.prnt("* " + _("Debug enabled"))
	return hexchat.EAT_ALL

""" Print info about translation """
def TransInfo(ctxt):
	ctxt.prnt("\n" + textBold("----------- " + _("Translation Status") + " -----------"))
	if TRANSLATING:
		if TRANSDIRECTION == "both":
			directions = _("both directions.")
		elif TRANSDIRECTION == "in":
			directions = _("incomming messages only.")
		else:
			directions = _("outgoing messages only.")
		ctxt.prnt("* " + _("Translation is") + " \x0303" + _("enabled") + " \017" + _("for") + " " + directions)
		ctxt.prnt("* " + _("Home language is set to") + " »" + re.sub('-' ,'_' , HOMELANG) + "«, " + _("foreign language is") + " »" + re.sub('-' ,'_' , FOREIGNLANG) + "«.")
		ctxt.prnt("* " + _("Display of original text is") + " " + _("enabled") + "." if SHOWORIGINAL else "* " + _("Display of original text is") + " " + _("disabled") + ".")
		ctxt.prnt("* " + _("Transmission of original text is") + " " + _("enabled") + "." if TRANSMITORIGINAL else "* " + _("Transmission of original text is") + " " + _("disabled") +".")
	else:
		ctxt.prnt("* " + _("Translation is") + " \x0304" + _("disabled") + "\017.")
	if DEBUG:
		ctxt.prnt("* " + _("Debug enabled"))
	return hexchat.EAT_ALL

""" Print help summary """
def help(ctxt):
	ctxt.prnt("\n" + textBold("------------------ " + _("Help") + " ------------------"))
	ctxt.prnt("\n" + textBold("\037") + _("Encryption") + (":\017"))
	ctxt.prnt("/enc enable . . . . . . . " + _("Encrypt outgoing messages on current")
		+ "\n                          " + _("private dialog window."))
	ctxt.prnt("/enc disable  . . . . . . " + _("Disable encryption of outgoing messages")
		+ "\n                          " + _("on current private dialog window."))
	ctxt.prnt("/enc status . . . . . . . " + _("Print info about status of encryption"))
	ctxt.prnt("/enc key \035‹nick›\017 \035‹key›\017 . . " + _("Store encryption key for nick of an interlocutor"))
	ctxt.prnt("/enc setkey \035‹key›\017 . . . . " + _("Store encryption key for nick of recent")
		+ "\n                          " + _("interlocutor.") + " (" + _("In private dialog windows only.") + ")")
	ctxt.prnt("/enc makekey  . . . . . . " + _("Generate and store safe encryption key")
		+ "\n                          " + _("for nick of recent interlocutor") + " (" + _("In private")
		+ "\n                          " + _("dialog windows only.") + ")")	
	ctxt.prnt("/enc showkey [\035‹nick›\017] . . " + _("Show the key phrase of given nick. In private")
		+ "\n                          " + _("windows you may omit nick to see the key phrase")
		+ "\n                          " + _("for conversation with your recent interlocutor."))
	ctxt.prnt("\n" + _("Encryption is and can be activated in private dialog windows only."))
	ctxt.prnt(_("If Translation is activated also, messages are first translated and then both original and"))
	ctxt.prnt(_("translation sent encrypted. While receiving an encrypted message it will get decrypted"))
	ctxt.prnt(_("first, and then translated. See privacy Information below for additional considerations."))
	ctxt.prnt("\n" + textBold("\037" + _("Translation:") + "\017"))
	ctxt.prnt("/tra both . . . . . . . . " + _("Apply translation on incomming and outgoing")
		+ "\n                          " + _("messages both") + ". (" + _("default") + ")")
	ctxt.prnt("/tra in . . . . . . . . . " + _("Only translate incomming messages"))
	ctxt.prnt("/tra out  . . . . . . . . " + _("Only translate outgoing messages"))
	ctxt.prnt("/tra off  . . . . . . . . " + _("Disable translation of messages"))
	ctxt.prnt("/tra showorig . . . . . . " + _("Show original untranslated incomming")
		+ "\n                          " + _("message text also") + ". (" + _("default") + ")")
	ctxt.prnt("/tra noshoworig . . . . . " + _("Swallow (don't show) original")
		+ "\n                          " + _("untranslated incomming message text") + ".")
	ctxt.prnt("/tra sendorig . . . . . . " + _("Send both translated and original")
		+ "\n                          " + _("message text") + ". (" + _("default") + ")")
	ctxt.prnt("/tra nosendorig . . . . . " + _("Send only translated message text")
		+ ",\n                          " + _("swallow original text."))
	ctxt.prnt("/tra status . . . . . . . " + _("Print info about status of translation"))
	ctxt.prnt("/tra sethome \035‹ll_CC›\017  . . " + _("Set home language different from your")
		+ "\n                          " + _("system default. Use Unix style language")
		+ "\n                          " + _("identificators eg.") + " \035fr_BE\017, \035fil_FIL\017 " + _("or") + " \035el\017")
	ctxt.prnt("/tra setforeign \035‹ll_CC›\017 . " + _("Set foreign language different from default") + " (\035en\017)")
	ctxt.prnt("\n" + _("Translation is activated/deactivated for all Channel and all Dialog windows globally."))
	ctxt.prnt("\n" + textBold("\037" + _("Privacy Information:") + "\017"))
	ctxt.prnt(_("If translation is activated, all messagetext will be sent to the"))
	ctxt.prnt(_("default translation service provider your system is set up for (google in most cases)."))
	ctxt.prnt(textBold(_("This will breake privacy for messages which were sent or received encrypted originally.")))
	ctxt.prnt(_("You may want to install a local translation program with a command line client") + "\n" + _("for improved privacy. If not:"))
	ctxt.prnt(textBold(textNeg(_("Consider all translated messages to be public, even if you have sent/received them encrypted!"))))
	ctxt.prnt("\n\x0315\035»" + _("A snailmail letter envelope is only made of paper, but it serves its purpose completely. Even if you didn't use steel plates for this because that might be more secure, no one would think of sending their letters without an envelope. Think of the encryption used here as a simple envelope for your messages.") + "«\017")
		
	return hexchat.EAT_ALL

init()

